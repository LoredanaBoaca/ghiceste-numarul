﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GhicesteNumarul
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int solutie = rand.Next(100);
            int nr = -1;
            int nrIncercari = 1;
            bool gasit = false;
            int diferenta = 0;
            while (!gasit)
            {
                nr = int.Parse(Console.ReadLine());
                if (nr == solutie)
                {
                    gasit = true;
                    Console.WriteLine("Felicitari!ati ghicit. Numarul este:" + nr + ",iar numarul de incercari este de: " + nrIncercari);
                }
                else
                {
                    nrIncercari++;
                    if (nr < solutie)
                        diferenta = solutie - nr;
                    else
                        diferenta = nr - solutie;
                    if (diferenta <= 3)
                        Console.WriteLine("Foarte fierbinte");
                    else if (diferenta <= 5)
                        Console.WriteLine("fierbinte");
                    else if (diferenta <= 10)
                        Console.WriteLine("cald");
                    else if (diferenta <= 20)
                        Console.WriteLine("caldut");
                    else if (diferenta <= 50)
                        Console.WriteLine("rece");
                    else if (diferenta > 50)
                        Console.WriteLine("foarte rece");
                }
            }

            Console.ReadKey();

        }
    }
}
